/*
 *  cloudmqtt_led.ino
 *  Copyright (c) 2016 Masami Yamakawa
 *  setupWifi(), reconnect() and parts of callback() functions are
 *    Copyright (c) 2008-2015 Nicholas O'Leary
 *  This software is released under the MIT License.
 *  http://opensource.org/licenses/mit-license.php
 *  
 *  An example to demonstrate analogRead and analogWrite over MQTT.
 *  cloudmqtt_led.ino subscriber
 *  cloudmqtt_light.ino publisher
 */

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

const char* ssid = "?????";
const char* password = "?????";
const char* mqttServer = "???.cloudmqtt.com";
const char* mqttClientId = "WSPOC002";
const char* mqttUser = "?????";
const char* mqttPassword = "?????";
const char* mqttTopic = "school/light";
const int mqttPort = ?????; //TLS port

WiFiClientSecure espClient;
PubSubClient client(espClient);

void setup() {
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  
  setupWifi();
  espClient.setInsecure();
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
}

void setupWifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  // JSON形式で送られたメッセージの中身を取り出すためのバッファを準備
  StaticJsonDocument<200> jsonBuffer;
  // JSON形式の中身を取り出して、rootに入れる
  DeserializationError error = deserializeJson(jsonBuffer, (char*) payload);

  // rootからhikariを値を取り出してinValueに入れる
  int  inValue = jsonBuffer["hikari"];

  Serial.print("inValue:");
  Serial.println(inValue);

  analogWrite(13,inValue);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(mqttClientId, mqttUser, mqttPassword)) {
      Serial.println("connected");
      client.subscribe(mqttTopic);
      Serial.print("Subscribe:");
      Serial.println(mqttTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
