/*
 *  cloudmqtt_light_sleep.ino
 *  Copyright (c) 2016 Masami Yamakawa
 *  setupWifi() and reconnect() functions are Copyright (c) 2008-2015 Nicholas O'Leary
 *  This software is released under the MIT License.
 *  http://opensource.org/licenses/mit-license.php
 * 
 *  Wire IO16(XPD)-RST
 *       VDD-R33K-NJL7302L-TOUT-R10K-GND
 *       VDD-R10K-RST
 *  An example to demonstrate analogRead and analogWrite over MQTT.
 *  cloudmqtt_led.ino subscriber
 *  cloudmqtt_light(_sleep).ino publisher
 */

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "?????";
const char* password = "?????";
const char* mqttServer = "???.cloudmqtt.com";
const char* mqttDeviceId = "WSPOC001";
const char* mqttUser = "?????";
const char* mqttPassword = "?????";
const char* mqttTopic = "school/light";
const int mqtt_port = ?????;
const unsigned long updateIntervalMicros = 10*1000*1000;

//Connect through TLS1.1
WiFiClientSecure espClient;
PubSubClient client(espClient);
unsigned long lastUpdateMillis = 0;
unsigned int value = 0;

void setup() {
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  // WiFi接続
  setupWifi();
  espClient.setInsecure();
  client.setServer(mqttServer, mqtt_port);
  
  // MQTTサーバーへ接続
  reconnect();
  
  // MQTTサーバーへ投稿をアップデート
  update();
  
  // MQTTサーバーから切断
  delay(1000);
  client.disconnect();
  Serial.println("Sleep...");
  // updateIntervalMicrosマイクロ秒間深い眠り（省電力）に
  ESP.deepSleep(updateIntervalMicros);
}

void setupWifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(mqttDeviceId, mqttUser, mqttPassword)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void update() {
  value=analogRead(A0);
  String payload = "{\"myName\":\"";
  payload += mqttDeviceId;
  payload += "\",\"hikari\":";
  payload += value;
  payload += "}";
  Serial.print("Publish message: ");
  Serial.println(payload);
  client.publish(mqttTopic, (char*) payload.c_str());
}

void loop() {
}
