/*
    cloudmqtt_light.ino
    Copyright (c) 2016 Masami Yamakawa
    setupWifi() and reconnect() functions are Copyright (c) 2008-2015 Nicholas O'Leary
    This software is released under the MIT License.
    http://opensource.org/licenses/mit-license.php

    An example to demonstrate analogRead and analogWrite over MQTT.
    cloudmqtt_led.ino subscriber
    cloudmqtt_light.ino publisher
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "WIFI_SSID";
const char* password = "WIFI_PASSWORD";
const char* mqttServer = "io.adafruit.com";
const char* mqttUser = "IO_USER";
const char* mqttPassword = "IO_TOKEN";
const char* mqttPubTopic = "IO_USER/f/light";
const char* mqttSubTopic = "IO_USER/f/led";
const char* mqttFingerprint = "7700542ddae7d80327312399eb27dbcba54c5718";
const int mqtt_port = 8883;
unsigned long updateIntervalMillis = 5 * 1000;

//Connect through TLS1.1
WiFiClientSecure espClient;
PubSubClient client(espClient);
unsigned long lastUpdateMillis = 0;
unsigned int value = 0;

String mqttDeviceId;

void setup() {
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  pinMode(13, OUTPUT);
  setupWifi();
  client.setServer(mqttServer, mqtt_port);
  // MQTTクライアント機能に、サーバーからメッセージを受信したときに実行する部分を設定
  client.setCallback(callback);
  mqttDeviceId = String(ESP.getChipId(), HEX);
  espClient.allowSelfSignedCerts();
  espClient.setFingerprint(mqttFingerprint);
}

void setupWifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(mqttDeviceId.c_str(), mqttUser, mqttPassword)) {
      Serial.println("connected");
      client.subscribe(mqttSubTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// MQTTサーバーからメッセージを受信したときに実行させる部分
void callback(char* topic, byte* payload, unsigned int length) {
  // Serial・・・の行はプログラムの主要部分でない
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // 先頭が「1」のメッセージを受信するとLEDをONにする
  if ((char)payload[0] == '1') {
    digitalWrite(13, HIGH);
  } else { //でなければ
    digitalWrite(13, LOW);
  }
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  unsigned long now = millis();
  if (now - lastUpdateMillis > updateIntervalMillis) {
    lastUpdateMillis = now;
    value = analogRead(A0);
    // payloadに{"value":センサー値}をセット
    String payload = "{\"value\":";
    payload += value;
    payload += "}";
    Serial.print("Publish message: ");
    Serial.println(payload);
    // payloadにセットされたJSON形式メッセージを投稿
    client.publish(mqttPubTopic, (char*) payload.c_str());
  }
}
